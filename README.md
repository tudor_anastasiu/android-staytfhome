# Corona Map App
This is a fullstack location-enabled app on Android for COVID-19 coronavirus pandemic tracking to enable users to self-report symptoms and test result (if taken). It is intended as a more fine-grained information source than Worldometer, where other people can look up the map of their city and know which hotspots to avoid.

The frontend is a native Android app written entirely in Kotlin language using the latest standards and libraries in the industry such as MVVM, OkHttp3/Retrofit2, Dagger 2 and 
Android Jetpack.

The back end is a REST API developed fully using Scala language and powered by Play framework and it has all the goods: signup, signin, account activation, CRUD for entries, security etc.


This repository contains the codebase for the Android app, check the repository for the backend [here](https://bitbucket.org/tudor_anastasiu/play-scala-staytfhome/src/master/).