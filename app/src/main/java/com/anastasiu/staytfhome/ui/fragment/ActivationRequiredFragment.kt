package com.anastasiu.staytfhome.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.anastasiu.staytfhome.R
import kotlinx.android.synthetic.main.fragment_activation_required.*

class ActivationRequiredFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_activation_required, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btSignIn.setOnClickListener {
            findNavController().navigate(ActivationRequiredFragmentDirections.actionActivationRequiredFragmentToSigninFragment())
        }
        btOpenEmail.setOnClickListener {
            val email = Intent(Intent.ACTION_MAIN)
            email.addCategory(Intent.CATEGORY_APP_EMAIL)
            startActivity(email)
        }
    }
}
