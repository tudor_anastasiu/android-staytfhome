package com.anastasiu.staytfhome.data.utils.interceptors

import android.content.Context
import android.preference.PreferenceManager
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response
import java.io.IOException

class AddAuthenticatorCookieInterceptor(private val context: Context) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Chain): Response {
        val builder = chain.request().newBuilder()
        val storedCookie = PreferenceManager.getDefaultSharedPreferences(context)
            .getString(PREF_COOKIES, "")
        // Use the following if you need everything in one line.
// Some APIs die if you do it differently.
        if (storedCookie != "") {
            builder.addHeader("Cookie", storedCookie!!)
        }
        return chain.proceed(builder.build())
    }

    companion object {
        const val PREF_COOKIES = "PREF_COOKIES"
    }

}