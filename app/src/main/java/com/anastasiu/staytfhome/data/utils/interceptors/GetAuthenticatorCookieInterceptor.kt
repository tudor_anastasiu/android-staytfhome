package com.anastasiu.staytfhome.data.utils.interceptors

import android.content.Context
import android.preference.PreferenceManager
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response
import java.io.IOException

class GetAuthenticatorCookieInterceptor(private val context: Context) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Chain): Response {
        val originalResponse = chain.proceed(chain.request())
        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            val cookieHeader = originalResponse.header("Set-Cookie")
            val memes = PreferenceManager.getDefaultSharedPreferences(context).edit()
            memes.remove(PREF_COOKIES).apply()
            memes.putString(PREF_COOKIES, cookieHeader)
                .apply()
            memes.commit()
        }
        return originalResponse
    }

    companion object {
        const val PREF_COOKIES = "PREF_COOKIES"
    }

}